package Lab5.ex1;

public class Program {
    public static void main(String[] args) {
        Circle circle = new Circle(6, "red", true);
        Rectangle rectangle = new Rectangle(3, 2, "magenta", false);
        Square square = new Square(8, "green", true);

        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println(square);
    }
}

