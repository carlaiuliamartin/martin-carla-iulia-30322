package Lab5.ex2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private boolean isRotated;

    public ProxyImage(String fileName, boolean isRotated){
        this.fileName = fileName;
        this.isRotated = isRotated;
    }

    @Override
    public void display() {
        if (!isRotated) {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        }
        else {
            if (rotatedImage == null) {
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }
    }
}
