package Lab5.ex2;

public class Programex2 {
    public static void main(String[] args) {
        Image image = new ProxyImage("image.jpg", false);
        Image image1 = new ProxyImage("image1.jpg", true);

        image.display();
        image1.display();
    }
}
