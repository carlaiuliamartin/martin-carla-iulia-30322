package Lab5.ex4;

import Lab5.ex3.LightSensor;
import Lab5.ex3.TemperatureSensor;

public class Controller {

    private static volatile Controller controller = null;
    TemperatureSensor tempSensor;
    LightSensor lightSensor;

    private Controller(){}

    private Controller(TemperatureSensor temperatureSensor, LightSensor lightSensor) {
        this.tempSensor = new TemperatureSensor();
        this.lightSensor = new LightSensor();
    }

    public static Controller getController()
    {
        synchronized (Controller.class){
            if(controller == null){
                controller = new Controller();
            }
        }
        return controller;
    }

}

