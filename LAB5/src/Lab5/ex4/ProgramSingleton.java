package Lab5.ex4;


import Lab5.ex3.Controller;
import Lab5.ex3.LightSensor;
import Lab5.ex3.TemperatureSensor;

public class ProgramSingleton {
    public static void main(String[] args) {
        TemperatureSensor t1 = new TemperatureSensor();
        LightSensor s1 = new LightSensor();

        Lab5.ex3.Controller c1 = new Controller(t1,s1);
        c1.control();
    }

