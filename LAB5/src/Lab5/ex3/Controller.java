package Lab5.ex3;

public class Controller {
    public TemperatureSensor tempSensor;
    public LightSensor lightSensor;
    public int time = 20;

    public Controller(){
        this.lightSensor = new LightSensor();
        this.tempSensor = new TemperatureSensor();
    }

    public void control(){

        for(int i = 1; i <= time; i++) {
            System.out.println("Second "+i+" Light sensor: "+lightSensor.readValue());
            System.out.println("Temperature sensor: "+tempSensor.readValue());
        }
    }

    public static void main(String[] args){

        Controller c = new Controller();
        c.control();
    }

}
