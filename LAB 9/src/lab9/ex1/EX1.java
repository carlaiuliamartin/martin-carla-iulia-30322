package lab9.ex1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EX1 {
    public static void main(String[] args) {

        JFrame frame = new JFrame("Exercise 2");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(300, 100);
        frame.setLayout(new GridLayout(1, 2));
        frame.setLocationRelativeTo(null);

        JButton button = new JButton("Button");
        button.setFont(new Font("Arial", Font.BOLD, 20));
        frame.add(button);

        JTextField textField = new JTextField();
        textField.setFont(new Font("Arial", Font.PLAIN, 20));
        textField.setHorizontalAlignment(JTextField.CENTER);
        frame.add(textField);

        button.addActionListener(new ClickButton(textField));

        frame.setVisible(true);
    }

    public static class ClickButton implements ActionListener {
        JTextField textField;
        int counter = 0;

        ClickButton(JTextField textField) {
            this.textField = textField;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            counter++;
            textField.setText(String.valueOf(counter));
        }
    }
}