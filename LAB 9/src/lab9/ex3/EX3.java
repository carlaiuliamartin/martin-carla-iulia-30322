package lab9.ex3;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class EX3 implements ActionListener {
    JFrame frame = new JFrame("Tic Tac Toe");
    JPanel textPanel = new JPanel();
    JTextField textField = new JTextField(20);
    JPanel boardPanel = new JPanel();
    JButton[] buttons = new JButton[9];

    private boolean pl1_chance;
    int count = 0;

    public EX3() {
        initBoard();
        startGame();
    }

    public void initBoard() {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLayout(new BorderLayout());
        frame.setLocationRelativeTo(null);

        textPanel.setLayout(new BorderLayout());

        textField.setFont(new Font("Arial", Font.PLAIN, 30));
        textField.setBackground(Color.BLACK);
        textField.setForeground(Color.WHITE);
        textField.setHorizontalAlignment(JTextField.CENTER);
        textPanel.add(textField, BorderLayout.CENTER);

        boardPanel.setLayout(new GridLayout(3, 3));

        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new JButton("");
            buttons[i].setFont(new Font("Arial", Font.BOLD, 60));
            buttons[i].setBackground(Color.WHITE);
            buttons[i].addActionListener(this);
            boardPanel.add(buttons[i]);
        }

        frame.add(textPanel, BorderLayout.NORTH);
        frame.add(boardPanel, BorderLayout.CENTER);

        frame.setVisible(true);
    }

    public void startGame() {
        Random random = new Random();
        int chance = random.nextInt(10);

        if (chance%2 == 0) {
            pl1_chance = true;
            textField.setText("X's Turn:");
        }
        else {
            pl1_chance = false;
            textField.setText("O's Turn:");
        }
    }

    public void gameOver(String str) {
        count = 0;

        Object[] option = {"Restart","Exit"};
        int n = JOptionPane.showOptionDialog(frame,"Game Over: " + str, "Game Over",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                null, option, option[0]);

        frame.dispose();
        if (n == 0) {
            new EX3();
        }
    }

    public void checkwinner() {
        if((buttons[0].getText().equals("X") && buttons[1].getText().equals("X") && buttons[2].getText().equals("X")) ||
                (buttons[3].getText().equals("X") && buttons[4].getText().equals("X") && buttons[5].getText().equals("X")) ||
                (buttons[6].getText().equals("X") && buttons[7].getText().equals("X") && buttons[8].getText().equals("X")) ||
                (buttons[0].getText().equals("X") && buttons[3].getText().equals("X") && buttons[6].getText().equals("X")) ||
                (buttons[1].getText().equals("X") && buttons[4].getText().equals("X") && buttons[7].getText().equals("X")) ||
                (buttons[2].getText().equals("X") && buttons[5].getText().equals("X") && buttons[8].getText().equals("X")) ||
                (buttons[0].getText().equals("X") && buttons[4].getText().equals("X") && buttons[8].getText().equals("X")) ||
                (buttons[2].getText().equals("X") && buttons[4].getText().equals("X") && buttons[6].getText().equals("X"))) {
            xWins();
        }
        else if((buttons[0].getText().equals("O") && buttons[1].getText().equals("O") && buttons[2].getText().equals("O")) ||
                (buttons[3].getText().equals("O") && buttons[4].getText().equals("O") && buttons[5].getText().equals("O")) ||
                (buttons[6].getText().equals("O") && buttons[7].getText().equals("O") && buttons[8].getText().equals("O")) ||
                (buttons[0].getText().equals("O") && buttons[3].getText().equals("O") && buttons[6].getText().equals("O")) ||
                (buttons[1].getText().equals("O") && buttons[4].getText().equals("O") && buttons[7].getText().equals("O")) ||
                (buttons[2].getText().equals("O") && buttons[5].getText().equals("O") && buttons[8].getText().equals("O")) ||
                (buttons[0].getText().equals("O") && buttons[4].getText().equals("O") && buttons[8].getText().equals("O")) ||
                (buttons[2].getText().equals("O") && buttons[4].getText().equals("O") && buttons[6].getText().equals("O"))) {
            oWins();
        }
        else if (count == 9) {
            textField.setText("Match Tie!");
            gameOver("Match Tie!");
        }
    }

    public void xWins() {
        textField.setText("X Wins!");
        gameOver("X Wins!");
    }

    public void oWins() {
        textField.setText("O Wins!");
        gameOver("O Wins!");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < 9; i++) {
            if (e.getSource() == buttons[i]) {
                if (pl1_chance) {
                    if (buttons[i].getText().equals("")) {
                        buttons[i].setText("X");
                        pl1_chance = false;
                        textField.setText("O's Turn:");
                        count++;
                        checkwinner();
                    }
                }
                else {
                    if (buttons[i].getText().equals("")) {
                        buttons[i].setText("O");
                        pl1_chance = true;
                        textField.setText("X's Turn:");
                        count++;
                        checkwinner();
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        new EX3();
    }
}
