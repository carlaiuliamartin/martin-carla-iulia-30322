package ex1;

import javax.swing.*;

public class Main extends JFrame {

    Main(View view){
        this.setSize(500,500);
        add(view);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        Sensor s = new Sensor();
        s.start();

        View view = new View();
        SensorController controller = new SensorController(s,view);
        new Main(view);
    }

}