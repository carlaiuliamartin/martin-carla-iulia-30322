package ex1;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class View extends JPanel implements Observer {
    JTextField textValue;
    JTextField notifier;

    View(){
        this.setLayout(new FlowLayout());
        textValue = new JTextField("20");
        textValue.setEditable(false);
        notifier = new JTextField();
        notifier.setBounds(0,200,400,200);
        notifier.setEditable(false);
        add(textValue);
        add(notifier);
    }

    public void update(Observable o, Object arg){
        String s = ""+((Sensor)o).getValue();
        textValue.setText(s);
        notifier.setText(s);
    }
}