package ex1;

import java.util.Observable;

public class Sensor extends Observable implements Runnable{

    private int value=20;
    boolean active = true;
    Thread t;

    public void start(){
        if(t==null){
            t = new Thread(this);
            t.start();
        }
    }

    public void run(){
        while(active){
            synchronized(this){
                double newValue = Math.random()*50+10;
                this.setValue((int)newValue);
                try {

                    Thread.sleep(1000);
                } catch (InterruptedException e) { }
                this.setChanged();
                this.notifyObservers();
            }
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        notify();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}