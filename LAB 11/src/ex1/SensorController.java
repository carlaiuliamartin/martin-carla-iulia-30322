package ex1;

import java.util.Observable;

public class SensorController {
    Sensor s;
    View view;

    public SensorController(Sensor s, View view){
        s.addObserver(view);
        this.s=s;
        this.view=view;
    }
}