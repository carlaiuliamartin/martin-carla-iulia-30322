package edu.utcn.lab12.vehicles;

public class Vehicle implements Comparable{

    private String type;
    private int weight;

    public Vehicle(String type, int length) {
        this.type = type;
    }
    
    private int getWeight() {
    	return this.weight;
    }
    
    @Override
    public int compareTo(Object o) {
        Vehicle v = (Vehicle) o;
        return weight - v.getWeight();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return weight == vehicle.weight &&
                type.equals(vehicle.type);
    }

    public String start(){
        return "electric engine started";
    }

}
