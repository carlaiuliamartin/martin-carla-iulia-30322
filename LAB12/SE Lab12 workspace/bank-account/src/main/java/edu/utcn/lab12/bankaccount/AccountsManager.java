package edu.utcn.lab12.bankaccount;

import java.util.ArrayList;

public class AccountsManager {
	ArrayList<BankAccount> bA = new ArrayList<BankAccount>();
	
    public void addAccount(BankAccount account){
    	bA.add(account);
    }

    public boolean exists(String id){
    	for(BankAccount b : bA ) {	
    		if(id.equals(b.getId()))
    			return true;
    	}
        return false;
    }

    public int count(){
        return bA.size();
    }
}
