package ex2;


import java.io.*;
import java.util.Scanner;

public class CharCounter {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        System.out.println("What char are you searching?");
        char myChar = scanner.next().charAt(0);
        int count = 0;
        int c = 0;

        File myFile = new File("LAB7/src/ex2/data.txt");
        FileReader reader = new FileReader(myFile);
        BufferedReader bufferedReader = new BufferedReader(reader);

        while((c = bufferedReader.read()) != -1)
        {
            char character = (char) c;
            if(character == myChar)
            {
                ++count;
            }
        }
        System.out.println("Number of " + myChar + " is: " + count);
    }
}