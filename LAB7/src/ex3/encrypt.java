package ex3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

class encryptor {
    // function to read from file test.txt
    public static String readFromFile(String fileName) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    } //.class

    // function to shift left every character in the string by 1
    public static String encrypt(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= 'a' && c <= 'z') {
                if (c == 'z') {
                    sb.append('a');
                } else {
                    sb.append((char) (c + 1));
                }
            } else if (c >= 'A' && c <= 'Z') {
                if (c == 'Z') {
                    sb.append('A');
                } else {
                    sb.append((char) (c + 1));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    // function to shift right every character in the string by 1
    public static String decrypt(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= 'a' && c <= 'z') {
                if (c == 'a') {
                    sb.append('z');
                } else {
                    sb.append((char) (c - 1));
                }
            } else if (c >= 'A' && c <= 'Z') {
                if (c == 'A') {
                    sb.append('Z');
                } else {
                    sb.append((char) (c - 1));
                }
            } else sb.append(c);
        }
        return sb.toString();
    }
    // test
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // give path to file as argument
        String fileName = args[0];
        // read from file
        int choose = -1;
        System.out.println("Do you want to encrypt or decrypt? (1/2)");
        // read choose from user

        String content = encryptor.readFromFile(fileName);
        // switch statement to encrypt and decrypt by choice
        while(choose != 0){
            System.out.println("1. Encrypt");
            System.out.println("2. Decrypt");
            System.out.println("0. Exit");
            System.out.println("Enter your choice: ");
            choose = scanner.nextInt();
            switch (choose) {
                case 1:
                    try {
                        Files.write(
                                Paths.get("LAB7/src/ex2/data.txt"),
                                encryptor.encrypt(content).getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    String content2 = encryptor.readFromFile("LAB7/src/ex2/data.txt");
                    try {
                        Files.write(
                                Paths.get("LAB7/src/ex2/data.txt"),
                                encryptor.decrypt(content2).getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 0:
                    System.out.println("Bye!");
                    break;
                default:
                    System.out.println("Invalid choice!");
                    break;
            }
        }
    }
}
