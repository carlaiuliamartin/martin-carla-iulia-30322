package carla.martin.lab2.ex1;
import java.util.*;
import java.util.Scanner;

public class EX3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter A : ");
        int A = scan.nextInt();
        System.out.println("Enter B : ");
        int B = scan.nextInt();
        boolean ok;
        int sum = 0;
        int i = 0;
        int j = 0;

       while(A<B) {
           ok = false;

           for (i = 2; i <= A / 2; ++i)
           {
               if(A %i == 0)
               {
                   ok = true;
                   break;
               }
           }
           if(!ok && A != 0 && A != 1)
               System.out.print(A + " ");
           ++B;
       }

    }
}