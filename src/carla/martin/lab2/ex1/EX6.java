package carla.martin.lab2.ex1;
import java.util.*;
import java.util.Scanner;
import java.util.Random;


public class EX6 {

    static int nonrecoursiveFactorial(int n)
    {
      if(n <= 2)
          return n;
      return n * recoursiveFactorial(n - 1);
    }

    static int recoursiveFactorial(int n)
    {
        int factorial = 1;
        for(int i = 1 ; i <= n; i++)
            factorial = factorial * i;
        return factorial;
    }
    public static void main(String[] args)
    {
       Scanner read  = new Scanner(System.in);
       int n = read.nextInt();

       System.out.println( + recoursiveFactorial(n));
       System.out.println( + nonrecoursiveFactorial(n));
    }

}
