package carla.martin.lab2.ex1;
import java.util.*;
import java.util.Scanner;
import java.util.Random;

public class EX5 {

    public static void bubbleSort(int[] x)
    {
        int n = x.length;
        for(int i = 0 ; i < n; i++)
        {
            for(int j = 0; j < n-i ; j ++)
            {
                if(x[j] == x[j+1])
                {
                    int aux = x[j];
                    x[j] = x[j+i];
                    x[j+i] = aux;

                }
            }
        }
    }


    public static void main(String[] args) {

        Random rand = new Random();
        int i;
        int[] x = new int[10];

        for (i = 0; i < 10; i++) {
            x[i] = rand.nextInt(100000);
            System.out.println(x[i]);
        }

        for (int p : x) {
            System.out.print(p + " ");
        }

        bubbleSort(x);
        System.out.print("\nNew a:");
        for (int p : x) {
            System.out.print(p + " ");
        }
    }
}
