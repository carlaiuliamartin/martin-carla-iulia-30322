package carla.martin.lab2.ex1;
import java.util.*;
import java.util.Scanner;
import java.util.Random;

public class EX7 {

    public static void main(String[] args)
    {
        Random rand = new Random();
        Scanner read = new Scanner(System.in);

        int a = rand.nextInt(5);
        int attemptsNumber = 0;
        int b = 0;
        while(attemptsNumber < 3 && b!= a)
        {
            b = read.nextInt();
            if(b > a)
            {
                System.out.println("Wrong answer, your number it too high");
                attemptsNumber++;
            }
            else if(b < a)
            {
                System.out.println("Wrong answer, your number is too low");
                attemptsNumber++;
            }
            else
                System.out.println("Number correct !");
        }

        if(attemptsNumber == 3)
            System.out.println("You los");

    }
}
