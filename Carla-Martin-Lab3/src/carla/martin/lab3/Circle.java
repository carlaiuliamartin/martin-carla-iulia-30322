package carla.martin.lab3;
import java.util.Scanner;
import java.util.Random;
import java.util.*;

public class Circle {

    double radius = 1.0;
    String color = "red";

    public Circle() {
    }

    public Circle(double raza, String culoare)
    {
        this.radius = raza;
        this.color = culoare;

    }

    public double getRadius()
    {
        return radius;
    }

    public String getColour() {
        return color;
    }

    public double getArea() {
        return Math.PI * this.radius * this.radius;
    }
    public static void main(String[] args) {
        Circle circle = new Circle(3.0, "pink");
        System.out.println("Colour is " + circle.getColour());
        System.out.println("Radius is " + circle.getRadius());
        System.out.println("Area is " + circle.getArea());
    }
}


