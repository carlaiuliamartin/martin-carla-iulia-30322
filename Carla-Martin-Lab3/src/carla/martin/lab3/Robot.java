package carla.martin.lab3;

public class Robot {

    int x;

    Robot()
    {
        x = 1;
    }
    void change(int k)
    {
        if(k>=1)
            x +=k;
    }

    public static void main(String[] args)
    {
        Robot value = new Robot();
        value.toString();
    }

    @Override
    public String toString(){
        return "x=" + x;
    }
}
