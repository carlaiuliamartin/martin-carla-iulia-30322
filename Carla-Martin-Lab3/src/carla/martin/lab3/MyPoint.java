package carla.martin.lab3;

public class MyPoint {
    double x;
    double y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(double _x, double _y) {
        this.x = _x;
        this.y = _y;

    }

    public double getX() {
        return x;

    }

    public void setX(double _x) {
        this.x = _x;

    }

    public double getY() {
        return y;
    }

    public void setY(double _y) {
        this.y = _y;
    }

    public void setXY(double _x, double _y) {
        this.x = _x;
        this.y = _y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";

    }

    public double calculateDistance(double toX, double toY) {
        return Math.sqrt((toX - this.getX()) * (toY - this.getY()) + (toX - this.getX()) * (toX - this.getX()));
    }

    public double calculateDistance(MyPoint to) {
        return this.calculateDistance(to.getX(), to.getY());
    }


    public static void main(String[] args) {
        MyPoint firstPoint = new MyPoint();
        MyPoint secondPoint = new MyPoint(9.0, 23.8);
        System.out.println("Distance from " + firstPoint + " to " + secondPoint + " is " + firstPoint.calculateDistance(secondPoint));
        System.out.println("Distance from " + firstPoint + " to (66.6, 77.7) is " + firstPoint.calculateDistance(66.6, 77.7));
    }
}
