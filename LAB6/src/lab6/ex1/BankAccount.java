package lab6.ex1;

import java.sql.SQLOutput;
import java.util.Objects;

public class BankAccount {
    public String owner;
    public double balance;


    public BankAccount(){
        this.owner = "";
        this.balance = 0;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void Withdraw(double amount)
    {
        if(amount > this.balance)
        {
            this.balance = balance - amount;
            System.out.println("Funds withdrawn!");
        }
        else
        {
            System.out.println("Insufficient ammount!");
        }
    }

    public void Deposit(double amount)
    {
        this.balance = balance + amount;
        System.out.println("Funds deposited!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    public double getBalance() {
        System.out.println("");
        return (double) balance;
    }

    public String getOwner() {
        System.out.println("");
        return owner;
    }
}