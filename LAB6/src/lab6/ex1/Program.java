package lab6.ex1;

    public class Program {
        public static void main(String[] args) {
            BankAccount b1 = new BankAccount("Claudiu",25);
            BankAccount b2 = new BankAccount("Aelx", 5);
            BankAccount b3 = new BankAccount("Popa", 10);
            BankAccount b4 = new BankAccount("Dan", 25);

            System.out.println(b2.equals(b3));
            System.out.println(b1.equals(b4));

            System.out.println(b1.hashCode());
            System.out.println(b2.hashCode());
            System.out.println(b4.hashCode());

            System.out.println("\n" + b2);
            b2.Deposit(10);
            System.out.println(b2);

            System.out.println("\n" + b3);
            b3.Withdraw(5);
            System.out.println(b3);
        }
    }
