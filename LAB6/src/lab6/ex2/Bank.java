package lab6.ex2;

import lab6.ex1.BankAccount;

import java.util.*;
import java.util.stream.Collectors;

public class Bank {

    private static ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

    public Bank() {
    }

    public Bank(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
    }


    public void addAccount(String owner, double balance) {
        BankAccount bb = new BankAccount(owner, balance);
        accounts.add(bb);
    }

    public static void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount b : accounts) {
            if (b.balance > minBalance && b.balance < maxBalance) {
                System.out.println("Account: " + b + "\n");
            }
        }
    }

    public void printAllAccounts(){
        // declare a local tree which sorts accounts by balance
        TreeSet<BankAccount> tmp = new TreeSet<BankAccount>(new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount o1, BankAccount o2) {
                return (int)(o1.getBalance() - o2.getBalance());
            }
        });

        tmp.addAll(accounts);

        System.out.println("Bank accounts sorted by balance: ");

        Iterator i = tmp.iterator();
        while(i.hasNext()){
            BankAccount bA = (BankAccount) i.next();
            System.out.println(bA);
        }
    }

    public ArrayList<BankAccount> getAllAccounts() {
        Collections.sort(accounts, new Comparator<BankAccount>(){
            public int compare(BankAccount b1, BankAccount b2) {
                return b1.getOwner().compareToIgnoreCase(b2.getOwner());
            }
        });
        return accounts;
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount b : accounts) {
            if (b.owner.equalsIgnoreCase(owner))
                return b;
        }
        return null;
    }
}
