package lab6.ex2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lab6.ex1.BankAccount;

public class Program {
    public static void main(String[] args) {

        Bank banca = new Bank();
        banca.addAccount("Maier", 20);
        banca.addAccount("Inter", 25);
        banca.addAccount("Lazar", 15);
        banca.addAccount("Oanar", 5);

        banca.printAllAccounts();

        System.out.println("\n");
        System.out.println(banca.getAccount("Maier"));
        System.out.println("\n");

        System.out.println("Accounts btw. a range: ");
        banca.printAccounts(10,22);

        System.out.println("Sorted alphabetically: ");
        System.out.println(banca.getAllAccounts());

    }
}
