package lab6.ex2;

public class BankAccount {
    String owner;
    double balance;

    public BankAccount(){
        this.owner = "";
        this.balance = 0;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void Withdraw(double amount)
    {
        balance -=amount;
        System.out.println("Money withdrawn!");
    }

    public void Deposit(double amount)
    {
        balance += amount;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
