package lab6.ex3;

public class BankAccount implements Comparable<BankAccount> {
    String owner;
    double balance;

    public BankAccount(){
        this.owner = "";
        this.balance = 0;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void Withdraw(double amount)
    {
        balance -=amount;
        System.out.println("Money withdrawn!");
    }

    public void Deposit(double amount)
    {
        balance += amount;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public int compareTo(BankAccount bnk) {
        return (int)(this.balance - bnk.balance);
    }


}