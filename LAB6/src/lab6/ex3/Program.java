package lab6.ex3;


public class Program {
    public static void main(String[] args) {

        Bank banca = new Bank();
        banca.addAccount("Inter", 20);
        banca.addAccount("Maier", 25);
        banca.addAccount("Clau", 15);
        banca.addAccount("Popa", 5);

        System.out.println("Print between a range");
        banca.printAccounts(10, 20);

        System.out.println("\nAll accounts based on balance: ");
        banca.printAccounts();

        System.out.println("\nClau's account: \n");
        System.out.println(banca.getAccount("Popa"));
    }
}