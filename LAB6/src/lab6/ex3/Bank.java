package lab6.ex3;

import java.util.*;

public class Bank  {

    private static TreeSet<BankAccount> accounts = new TreeSet<BankAccount>();

    public void addAccount(String owner, double balance) {
        BankAccount bb = new BankAccount(owner, balance);
        accounts.add(bb);
    }

    public static void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount b : accounts) {
            if (b.balance >= minBalance && b.balance <= maxBalance) {
                System.out.println("Account: " + b + "\n");
            }
        }
    }

    public void printAccounts() {
        for (BankAccount account : this.accounts) {
            System.out.println(account);
        }
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount b : accounts) {
            if (b.owner.equalsIgnoreCase(owner))
                return b;
        }
        return null;
    }
}
