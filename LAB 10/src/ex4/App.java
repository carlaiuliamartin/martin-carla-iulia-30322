package ex4;

import com.sun.javafx.scene.traversal.Direction;

import java.util.ArrayList;
import java.util.function.Predicate;


class App implements Runnable {
    static int BOARD = 4;
    private final ArrayList<Robot> robots = new ArrayList<>();

    public App() {
        this.robots.add(new Robot("One", this.generatePosition()));
        this.robots.add(new Robot("Two", this.generatePosition()));
        this.robots.add(new Robot("Three", this.generatePosition()));
        this.robots.add(new Robot("Four", this.generatePosition()));
        this.robots.add(new Robot("Five", this.generatePosition()));
        this.robots.add(new Robot("Six", this.generatePosition()));
        this.robots.add(new Robot("Seven", this.generatePosition()));
        this.robots.add(new Robot("Eight", this.generatePosition()));
        this.robots.add(new Robot("Nine", this.generatePosition()));
        this.robots.add(new Robot("Ten", this.generatePosition()));
    }

    private Position generatePosition() {
        return new Position((int) (Math.random() * BOARD), (int) (Math.random() * BOARD));
    }

    private Direction generateCorrectDirection(Robot robot) {
       var correct = true;
        Direction move;
        do {
            move = Direction.values()[(int) (Math.random() * 4)];
            switch (move) {
                case UP:
                    if (robot.getPosition().getY() + 1 < BOARD) {
                        correct = false;
                    }
                    break;
                case DOWN:
                    if (robot.getPosition().getY() - 1 >= 0) {
                        correct = false;
                    }
                    break;
                case LEFT:
                    if (robot.getPosition().getX() - 1 >= 0) {
                        correct = false;
                    }
                    break;
                case RIGHT:
                    if (robot.getPosition().getX() + 1 < BOARD) {
                        correct = false;
                    }
                    break;
            }
        } while (!correct);
        return move;
    }

    public void destroyRobot(Robot robot) {
        this.robots.remove(robot);
    }

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    public void displayBoard() {
        System.out.println("=============== BOARD ===============");
        System.out.println("Robots: " + this.robots.size());

        for (int i = 0; i < BOARD; i++) {
            for (int j = 0; j < BOARD; j++) {
                boolean found = false;
                for (Robot robot : this.robots) {
                    if (robot.getPosition().getX() == i && robot.getPosition().getY() == j) {
                        System.out.print("R");
                        found = true;
                    }
                }
                if (found) {
                    System.out.print("* ");
                } else {
                    System.out.print("- ");
                }
            }
            System.out.println();
        }
        System.out.println("=====================================");

    }

    public void run() {
        try {
            while (this.robots.size() > 1) {
                for (Robot robot : this.robots) {
                    var move = this.generateCorrectDirection(robot);
                    robot.move(move);
                }

                for (Robot robot : this.robots) {
                    var collided = this.robots.stream().anyMatch(Predicate.isEqual(robot));

                    if (collided) {
                        this.destroyRobot(robot);
                    }
                }

                this.displayBoard();
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}