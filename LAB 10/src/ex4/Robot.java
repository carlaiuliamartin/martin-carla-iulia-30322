package ex4;

public class Robot {
    private String name;
    private Position position;

    public Robot(String name, Position position) {
        this.name = name;
        this.position = position;
    }

    public Robot(String name) {
        this.name = name;
        this.position = new Position(0, 0);
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public enum Direction {
        UP, DOWN, LEFT, RIGHT
    }

    public void move(Direction move) {
        switch (move) {
            case UP:
                position.setY(position.getY() + 1);
                break;
            case DOWN:
                position.setY(position.getY() - 1);
                break;
            case LEFT:
                position.setX(position.getX() - 1);
                break;
            case RIGHT:
                position.setX(position.getX() + 1);
                break;
        }

    }

}