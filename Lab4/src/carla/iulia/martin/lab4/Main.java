package carla.iulia.martin.lab4;
import carla.iulia.martin.lab4.Circle;
import carla.iulia.martin.lab4.Rectangle;
import carla.iulia.martin.lab4.Square;
import carla.iulia.martin.lab4.Shape;


public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(10);
        Rectangle rectangle = new Rectangle(5, 6);
        Square square = new Square(8);

        System.out.println(circle + "Area is: " + circle.getArea() + " Perimeter is: " + circle.getPerimeter());
        System.out.println(rectangle + " Area is: " + rectangle.getArea() + " Perimeter is: " + rectangle.getPerimeter());
        System.out.println(square + " Area is: " + square.getArea() + " Perimeter is: " + square.getPerimeter());
    }
}