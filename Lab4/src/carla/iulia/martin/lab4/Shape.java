package carla.iulia.martin.lab4;

public class Shape {
    private String colour;
    private boolean filled;

    public Shape() {
        this.colour = "green";
        this.filled = true;
    }

    public Shape(String colour, boolean filled) {
        this.colour = colour;
        this.filled = filled;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        return "A shape coloured in " + getColour() + ", which is " + (isFilled() ? "filled" : "not filled");
    }

    }


