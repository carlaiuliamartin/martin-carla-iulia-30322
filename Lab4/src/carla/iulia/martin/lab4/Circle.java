package carla.iulia.martin.lab4;


public class Circle {
    private double radius;
    private String colour;

    public Circle() {
        this.radius = 1.0;
        this.colour = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
        this.colour = "red";
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * getRadius() * getRadius();
    }
    public double getPerimeter()
    {
        return 2*Math.PI*radius;
    }
    public static void main(String[] args) {
        Circle circle = new Circle(6);
        System.out.println("The circle radius is " + circle.getRadius() + " and its area is " + circle.getArea());
    }
}
