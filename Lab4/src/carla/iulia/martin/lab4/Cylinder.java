package carla.iulia.martin.lab4;

public class Cylinder extends Circle {
    private double height;

    public Cylinder() {
        super();
        this.height = 1.0;
    }

    public Cylinder(double radius) {
        super(radius);
        this.height = 1.0;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return height * getArea();
    }

    @Override
    public double getArea() {
        return 2 * Math.PI * getRadius() * getHeight() + 2 * super.getArea();
    }

    public static void main(String args[])
    {
        Cylinder cylinder = new Cylinder(26, 89);
        System.out.println("Area: " + cylinder.getArea());
        System.out.println("Volume: " + cylinder.getVolume());
    }
}